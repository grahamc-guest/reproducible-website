---
layout: new/blog
week: 200
---

* [lamby increased the diskspace and memory for buildinfo.debian.net](http://buildinfo.debian.net/)

* h01ger submitted us to https://www.outreachy.org/may-2019-august-2019-outreachy-internships/communities/debian/ and so far answered initial requests of 5 interested candidates.

* [FIXME](https://code.qt.io/cgit/qt/qtbase.git/commit/?id=1ffcca4cc208c48ddb06b6a23abf1756f9724351)
